// this includes Redux DevTools extension so that you can inspect the store while developing.
import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '../features/counter/counterSlice'

export const toolStore = configureStore({
    reducer: {
        counter: counterReducer
    },
})